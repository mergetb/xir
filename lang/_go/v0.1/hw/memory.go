package hw

type Dimm struct {
	Base
	Capacity uint64 `json:"capacity"`
}
