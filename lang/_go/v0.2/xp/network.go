package xp

type Network struct {
	Name    string
	Devices []Device
	Links   []Link
}
