from .. import ConstraintGenerator

cores = ConstraintGenerator("cores")
memory = ConstraintGenerator("memory")
image = ConstraintGenerator("image")
disk = ConstraintGenerator("disk")

metal = ConstraintGenerator("metal")
vm = ConstraintGenerator("vm")

arch = ConstraintGenerator("arch")
armv7 = "armv7"
x86_64 = "x86_64"

