from .pb import (
    Protocol,
)

http = Protocol.Name(Protocol.http)
https = Protocol.Name(Protocol.https)
tcp = Protocol.Name(Protocol.tcp)
udp = Protocol.Name(Protocol.udp)
