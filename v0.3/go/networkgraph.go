package xir

//
// graph.go
// --------
//
// Convert a xir.Network into a Gonum Graph type. See https://www.gonum.org.
//
// There are two versions of graphs that can be created: a NodeGraph and a
// SocketGraph. The NodeGraph's vertexes contain just xir.Network.Nodes. This
// graph is strictly a graph of nodes. The SocketGraph breaks out all Node.Sockets
// into graph vertexes as well. This graph can be used to traverse the network at
// the socket/interface level. The SocketGraph vertex then has two "types" a node
// and a socket.
//
// Each graph type supports graphviz/dot output.
//
// Network Node Id are just the Id of the Node. Socket Ids are of the form "x.n" where
// x is the Node Id and n is the Socket index. For example a Node "foo" with 2 sockets would
// look like:
//
//  "foo" --> "foo.0"
//    |
//    ------> "foo.1"
//
// In this example "foo" is a node type graph node and "foo.0" is a socket type graph node.
//
// To find the shortest path between nodes "a" and "b" using a SocketGraph you'd do the following:
//
// net := getXirNetwork()		// net is a xit.Network
// g := net.ToSocketGraph()     // g is a gonum Graph that has SocketGraphVertex vertexes
// a := g.NodeByName("a")	    // a is a SocketGraphVertex which implements gonum Graph.Node interface
// b := g.NodeByName("b")       // same as a
//
// p := path.path.DijkstraFrom(a, g)  // use the gonum path pkg and the graph.Nodes to find shortest path.
// hops, _ := p.To(b.ID())            // get the hops in the shortest path of the graph.
//
// (note that the hops contain a mixture of "node" nodes and "socket" nodes. confusing, I know.)
//
// Example dump of a shortest path:
//
//		c2|c2.0|cgw.0|cgw|cgw.1|r1.4|r1|r1.0|r0.0|r0|r0.1|a0.0|a0
//
// Note the ends of the paths are "node" nodes. They are the xir.Nodes "c2" and "a0". Also note
// that all nodes in the path are joined together by sockets.
//
import (
	"fmt"
	"strings"

	"gonum.org/v1/gonum/graph/encoding"
	"gonum.org/v1/gonum/graph/simple"
)

// NodeGraph builds a graph using the Nodes in a Network.
type NodeGraph struct {
	Network *Network

	gids map[string]int64 // map node name to node graph id.
	*simple.UndirectedGraph
}

// NodeGraphVertex is the Node itself.
type NodeGraphVertex struct {
	Node *Node
	gid  int64
}

// NodeGraphEdge has the endpoints and Link of the edge between the nodes.
type NodeGraphEdge struct {
	Link         *Link
	EndpointFrom *Endpoint
	EndpointTo   *Endpoint

	simple.Edge
}

// SocketGraph is a graph of the Sockets of all Nodes in a given Network. This
// is a graph of the interfaces ("sockets") of Nodes.
type SocketGraph struct {
	Network *Network

	gids map[string]int64 // map of socket id to graph id
	*simple.UndirectedGraph
}

// SocketGraphVertex holds the Socket and a pointer to the Node the Socket is a part of.
type SocketGraphVertex struct {
	Socket *Socket
	Node   *Node // Node that the socket is a part of.
	Link   *Link // Link that the socket is in.

	gid int64
}

// SocketGraphEdge just is a simple edge and contains nothing from the Network/Node/Socket.
type SocketGraphEdge struct {
	Network bool // if true, this is a network connection.
	simple.Edge
}

// LinkGraph has the links as vertexes and the nodes as edges
type LinkGraph struct {
	Network *Network

	gids map[string]int64
	*simple.UndirectedGraph
}

type LinkGraphVertex struct {
	// one of these will be set
	Node *Node
	Link *Link

	gid int64
}

type LinkGraphEdge struct {
	// both of these will be set
	Node *Node
	Link *Link

	simple.Edge
}

// implement simple.Graph interface
func (v NodeGraphVertex) ID() int64 {
	return v.gid
}

// implement simple.Graph interface
func (v SocketGraphVertex) ID() int64 {
	return v.gid
}

// implement simple.Graph interface
func (v LinkGraphVertex) ID() int64 {
	return v.gid
}

func (v SocketGraphVertex) String() string {
	if !v.IsNode() {
		return v.Socket.Label(v.Node.GetId())
	}

	return v.Node.GetId()
}

func (v LinkGraphVertex) String() string {
	if !v.IsNode() {
		return v.Link.GetId()
	}

	return v.Node.GetId()
}

func (v SocketGraphVertex) IsNode() bool {
	return v.Socket == nil
}

func (v LinkGraphVertex) IsNode() bool {
	return v.Link == nil
}

func NewNodeGraph(n *Network) *NodeGraph {
	return &NodeGraph{
		gids:            make(map[string]int64),
		Network:         n,
		UndirectedGraph: simple.NewUndirectedGraph(),
	}
}

func NewSocketGraph(n *Network) *SocketGraph {
	return &SocketGraph{
		gids:            make(map[string]int64),
		Network:         n,
		UndirectedGraph: simple.NewUndirectedGraph(),
	}
}

func NewLinkGraph(n *Network) *LinkGraph {
	return &LinkGraph{
		gids:            make(map[string]int64),
		Network:         n,
		UndirectedGraph: simple.NewUndirectedGraph(),
	}
}

// Get graph node given a xir.Node id (name)
func (ng *NodeGraph) NodeByName(name string) *NodeGraphVertex {

	id, ok := ng.gids[name]
	if !ok {
		return nil
	}

	n, ok := ng.UndirectedGraph.Node(id).(*NodeGraphVertex)
	if !ok {
		return nil
	}

	return n
}

// ToNodeGraph returns a graph with xir.Nodes as vertexes and xir.Endpoints as edges.
func ToNodeGraph(net *Network) *NodeGraph {

	g := NewNodeGraph(net)

	// add all nodes.
	for _, node := range net.GetNodes() {

		nid := g.UndirectedGraph.NewNode().ID()
		n := &NodeGraphVertex{
			gid:  nid,
			Node: node,
		}

		g.gids[node.Id] = nid // map node name to graph id

		g.UndirectedGraph.AddNode(n)

	}

	// iterate over all endpoints in a link and make two directional edges for each pair
	for _, link := range net.GetLinks() {

		for _, epa := range link.Endpoints {
			for _, epb := range link.Endpoints {

				if epa.Index == epb.Index { // skip linking to self
					continue
				}

				a := g.Node(g.gids[epa.Socket.Element]) // from node
				b := g.Node(g.gids[epb.Socket.Element]) // to node

				e := &NodeGraphEdge{
					Link:         link,
					EndpointFrom: epa,
					EndpointTo:   epb,
					Edge:         simple.Edge{F: a, T: b},
				}

				g.SetEdge(e)
			}
		}
	}

	return g
}

func (sg *SocketGraph) NodeByName(name string) *SocketGraphVertex {

	id, ok := sg.gids[name]
	if !ok {
		return nil
	}

	n, ok := sg.UndirectedGraph.Node(id).(*SocketGraphVertex)
	if !ok {
		return nil
	}

	return n
}

// Convert to a graph of all the Sockets in the Network
func ToSocketGraph(net *Network) *SocketGraph {

	g := NewSocketGraph(net)

	// add all nodes and sockets
	for _, node := range net.GetNodes() {

		nid := g.UndirectedGraph.NewNode().ID()
		n := &SocketGraphVertex{
			gid:    nid,
			Node:   node,
			Socket: nil,
		}

		g.gids[node.GetId()] = n.gid
		g.UndirectedGraph.AddNode(n)

		for _, socket := range node.GetSockets() {

			nid := g.UndirectedGraph.NewNode().ID()
			s := &SocketGraphVertex{
				gid:    nid,
				Node:   node,
				Socket: socket,
				// Link is filled in below when we interate the links.
			}

			sid := socket.Label(node.GetId())
			g.gids[sid] = s.gid // map socket name to graph id

			g.UndirectedGraph.AddNode(s)

			// now link node and socket
			f := g.Node(n.gid)
			t := g.Node(s.gid)

			e := &SocketGraphEdge{Network: false, Edge: simple.Edge{F: f, T: t}}
			g.SetEdge(e)
		}
	}

	// iterate over all sockets in a link and make two directional edges for each pair
	for _, link := range net.GetLinks() {

		for _, epa := range link.Endpoints {
			for _, epb := range link.Endpoints {

				if epa.Index == epb.Index { // skip linking to self
					continue
				}

				// Confusing, but this "node" is a socket. g.Vertex() would be a better way to put it.
				sid := epa.Socket.Label()
				a := g.Node(g.gids[sid]) // from node

				sid = epb.Socket.Label()
				b := g.Node(g.gids[sid]) // to node

				e := &SocketGraphEdge{
					Network: true,
					Edge:    simple.Edge{F: a, T: b},
				}

				g.SetEdge(e)

				// backfill the node Links as we're iterating them now.
				a.(*SocketGraphVertex).Link = link
				b.(*SocketGraphVertex).Link = link
			}
		}
	}

	return g
}

func (lg *LinkGraph) NodeByName(name string) *LinkGraphVertex {

	id, ok := lg.gids[name]
	if !ok {
		return nil
	}

	n, ok := lg.UndirectedGraph.Node(id).(*LinkGraphVertex)
	if !ok {
		return nil
	}

	return n
}

// Convert to a graph of all the Sockets in the Network
func ToLinkGraph(net *Network, includeLeaves bool) *LinkGraph {

	g := NewLinkGraph(net)

	socketToLink := make(map[string]*Link)

	// add all links as vertexes
	for _, link := range net.GetLinks() {

		nid := g.UndirectedGraph.NewNode().ID()
		n := &LinkGraphVertex{
			gid:  nid,
			Link: link,
		}

		g.gids[link.Id] = nid // map node name to graph id

		g.UndirectedGraph.AddNode(n)

		for _, ep := range link.Endpoints {
			socketToLink[ep.Socket.Label()] = link
		}

	}

	// iterate over all nodes and add edges
	for _, node := range net.GetNodes() {
		// it's possible for a node to have more than 1 socket in a link,
		// just take the first one
		var links []*Link
		seen := make(map[*Link]bool)
		for _, socket := range node.Sockets {
			link := socketToLink[socket.Label(node.Id)]
			if !seen[link] {
				seen[link] = true
				links = append(links, link)
			}
		}

		// only add node to the graph if it has more than 1 link,
		// as we don't care about routing to/from leaf/island nodes
		// (the connection is good enough)
		if !includeLeaves && len(links) <= 1 {
			continue
		}

		// from node
		nid := g.UndirectedGraph.NewNode().ID()
		n := &LinkGraphVertex{
			gid:  nid,
			Node: node,
		}

		g.gids[node.Id] = nid // map node name to graph id

		g.UndirectedGraph.AddNode(n)

		for _, link := range links {
			// from vertex
			l_v := g.Node(g.gids[link.Id])

			e := &LinkGraphEdge{
				Node: node,
				Link: link,
				Edge: simple.Edge{F: n, T: l_v},
			}

			g.SetEdge(e)

		}
	}

	return g
}

// graphviz interfaces
func (v NodeGraphVertex) DOTID() string {
	return v.Node.Id
}

func (e NodeGraphEdge) FromPort() (string, string) {
	return fmt.Sprintf("%d", e.EndpointFrom.Socket.Index), ""
}

func (e NodeGraphEdge) ToPort() (string, string) {
	return fmt.Sprintf("%d", e.EndpointTo.Socket.Index), ""
}

// dot vertex attribute interface. We add a label divided into
// an area per interface. The edges use these areas to point
// to the correct place on the vertex. Format for this is
// "<n> name|<n1> name1|...|<nx> namex>". The Mrecord shape
// gives us rounded corners on the vertex.
func (v NodeGraphVertex) Attributes() []encoding.Attribute {

	vs := []string{}
	for _, s := range v.Node.Sockets {
		vs = append(vs, fmt.Sprintf("<%d> %s", s.Index, s.Label(v.Node.GetId())))
	}

	label := strings.Join(vs, "|")

	return []encoding.Attribute{{
		Key: "shape", Value: "Mrecord",
	}, {
		Key: "label", Value: label,
	}}
}

func (v SocketGraphVertex) DOTID() string {
	return v.String()
}

func (v SocketGraphVertex) Attributes() []encoding.Attribute {

	if v.IsNode() {
		return []encoding.Attribute{{
			Key: "shape", Value: "square",
		}, {
			Key: "label", Value: v.Node.GetId(),
		}, {
			Key: "color", Value: "red",
		}}
	}

	addr := ""
	if v.Socket.Addrs != nil && len(v.Socket.Addrs) > 0 {
		addr = v.Socket.Addrs[0] // choose the first I guess...
	}

	label := fmt.Sprintf(
		"%s\n%s",
		v.Socket.Label(v.Node.GetId()),
		addr,
	)

	return []encoding.Attribute{{
		Key: "color", Value: "green",
	}, {
		Key: "label", Value: label,
	}}
}

func (v SocketGraphEdge) Attributes() []encoding.Attribute {

	attrs := []encoding.Attribute{}

	if !v.Network {
		attrs = append(attrs, encoding.Attribute{
			Key: "style", Value: "dashed",
		})
	}

	return attrs
}

// graphviz interfaces
func (v LinkGraphVertex) DOTID() string {
	return v.Link.Id
}

func (e LinkGraphEdge) FromPort() (string, string) {
	return e.Node.Id, ""
}

func (e LinkGraphEdge) ToPort() (string, string) {
	return e.Link.Id, ""
}

// dot vertex attribute interface. We add a label divided into
// an area per interface. The edges use these areas to point
// to the correct place on the vertex. Format for this is
// "<n> name|<n1> name1|...|<nx> namex>". The Mrecord shape
// gives us rounded corners on the vertex.
func (e LinkGraphEdge) Attributes() []encoding.Attribute {

	label := fmt.Sprintf("%s|%s", e.Node.Id, e.Link.Id)

	return []encoding.Attribute{{
		Key: "shape", Value: "Mrecord",
	}, {
		Key: "label", Value: label,
	}}
}

func (v LinkGraphVertex) Attributes() []encoding.Attribute {

	if v.IsNode() {
		return []encoding.Attribute{{
			Key: "shape", Value: "square",
		}, {
			Key: "label", Value: v.Node.Id,
		}, {
			Key: "color", Value: "red",
		}}
	}

	return []encoding.Attribute{{
		Key: "color", Value: "green",
	}, {
		Key: "label", Value: v.Link.Id,
	}}
}
