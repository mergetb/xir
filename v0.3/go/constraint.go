package xir

// The Satisfies family of checks only checks STATIC conditions, e.g. does a
// port have the right protocols. Dynamic checks that evaluate whether a
// consumable aspect of a resource such as bandwidht capacity are checked by the
// realization engine

func (n *NIC) Satisfies(s *NICSpec) bool {

	if s == nil {
		return true
	}

	/* TODO(ry)
	if s.Model != nil {
		switch s.Model.Op {
		case Operator_EQ:
			if n.Model != s.Model.Value {
				return false
			}
		case Operator_NE:
			if n.Model == s.Model.Value {
				return false
			}
		}
	}

	if s.DPDK != nil {
		switch s.DPDK.Op {
		case Operator_EQ:
			if n.DPDK != s.DPDK.Value {
				return false
			}
		case Operator_NE:
			if n.DPDK == s.DPDK.Value {
				return false
			}
		}
	}
	*/

	return true

}

// WantsVF returns true if s requires a VF
func (s *PortSpec) WantsVF() bool {
	if s.SriovVF == nil {
		return false // when ommitted, default is no-vfs
	}
	switch s.SriovVF.Op {
	case Operator_EQ:
		return s.SriovVF.Value
	case Operator_NE:
		return !s.SriovVF.Value
	default:
		return false // should give an error
	}
}

// SatisfiesSriovVF checks if the model requested VF and the port is a VF
func (p *Port) SatisfiesSriovVF(s *PortSpec) bool {
	port_is_vf := (p.Sriov != nil) && (p.Sriov.GetVf() != nil)
	spec_wants_vf := s.WantsVF()

	return port_is_vf == spec_wants_vf
}

func (p *Port) Satisfies(s *PortSpec) bool {

	if s.FormFactor != nil {
		switch s.FormFactor.Op {
		case Operator_EQ:
			if p.FormFactor != s.FormFactor.Value {
				return false
			}
		}
	}

	//TODO check protocols

	//check if SRIOV is needed
	if !p.SatisfiesSriovVF(s) {
		return false
	}
	return true
}

func (c *RoutingConstraint) Satisfies(value Routing) bool {
	switch c.Op {
	case Operator_EQ:
		return c.Value == value
	case Operator_NE:
		return c.Value != value
	}

	return false
}

func (c *AddressingConstraint) Satisfies(value Addressing) bool {
	switch c.Op {
	case Operator_EQ:
		return c.Value == value
	case Operator_NE:
		return c.Value != value
	}

	return false
}

func (c *EmulationConstraint) Satisfies(value Emulation) bool {
	switch c.Op {
	case Operator_EQ:
		return c.Value == value
	case Operator_NE:
		return c.Value != value
	}

	return false
}
