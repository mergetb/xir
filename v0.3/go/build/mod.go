package build

import (
	"fmt"

	"gitlab.com/mergetb/xir/v0.3/go"
)

type Modifier interface {
	Modify(x interface{})
}

type AllocModeMod struct {
	Modes []xir.AllocMode
}

func (a AllocModeMod) Modify(x interface{}) {

	r, ok := x.(*xir.Resource)
	if ok {
		r.Alloc = a.Modes
	}

}

func AllocModes(modes ...xir.AllocMode) AllocModeMod {
	return AllocModeMod{
		Modes: modes,
	}
}

type RoleMod struct {
	Roles []xir.Role
}

func (a RoleMod) Modify(x interface{}) {

	r, ok := x.(*xir.Resource)
	if ok {
		r.Roles = a.Roles
	}

}

func Roles(roles ...xir.Role) RoleMod {
	return RoleMod{
		Roles: roles,
	}
}

type RoleAddMod struct {
	Roles []xir.Role
}

func (a RoleAddMod) Modify(x interface{}) {
	r, ok := x.(*xir.Resource)
	if ok {
		r.Roles = append(r.Roles, a.Roles...)
	}
}

func RoleAdd(roles ...xir.Role) RoleAddMod {
	return RoleAddMod{
		Roles: roles,
	}
}

type DefaultImageMod struct {
	Image string
}

func (i DefaultImageMod) Modify(x interface{}) {
	r, ok := x.(*xir.Resource)
	if ok {
		if r.OS == nil {
			r.OS = new(xir.OSConfig)
		}
		r.OS.DefaultImage = i.Image
	}
}

func DefaultImage(img string) DefaultImageMod {
	return DefaultImageMod{
		Image: img,
	}
}

type RootdevMod struct {
	Rootdev string
}

func (m RootdevMod) Modify(x interface{}) {
	r, ok := x.(*xir.Resource)
	if ok {
		if r.OS == nil {
			r.OS = new(xir.OSConfig)
		}
		r.OS.Rootdev = m.Rootdev
	}
}

func Rootdev(dev string) RootdevMod {
	return RootdevMod{
		Rootdev: dev,
	}
}

const (
	ImagenetVNI uint32 = 3
	StornetVNI  uint32 = 4
)

type ServiceEndpointMod struct {
	Vni     uint32
	Address string
}

func (a ServiceEndpointMod) Modify(x interface{}) {
	r, ok := x.(*xir.Resource)
	if ok {
		if r.OS == nil {
			r.OS = new(xir.OSConfig)
		}
		if r.OS.ServiceEndpoints == nil {
			r.OS.ServiceEndpoints = make(map[uint32]*xir.ServiceEndpoint)
		}
		r.OS.ServiceEndpoints[a.Vni] = &xir.ServiceEndpoint{Address: a.Address}
	}
}

func ServiceEndpoint(vni uint32, address string) ServiceEndpointMod {
	return ServiceEndpointMod{
		Vni:     vni,
		Address: address,
	}
}

type RavenPowerControlMod struct {
	Host string
}

func (rpcm RavenPowerControlMod) Modify(x interface{}) {
	r, ok := x.(*xir.Resource)
	if ok {
		r.PowerControl = &xir.Resource_Raven{Raven: &xir.Raven{
			Host: rpcm.Host,
		}}
	}
}

func Raven(host string) RavenPowerControlMod {
	return RavenPowerControlMod{
		Host: host,
	}
}

type IpmiPowerControlMod struct {
}

func (ipcm IpmiPowerControlMod) Modify(x interface{}) {
	r, ok := x.(*xir.Resource)
	if ok {
		r.PowerControl = &xir.Resource_Ipmi{Ipmi: &xir.BMC{
			Kind: xir.BMC_IPMI,
			Host: fmt.Sprintf("%s-ipmi", r.Id),
		}}
	}
}

func BMCIpmi() IpmiPowerControlMod {
	return IpmiPowerControlMod{}
}

type CapacityMod struct {
	Value uint64
}

func (m *CapacityMod) Modify(x interface{}) {
	x.(xir.SetCapacity).SetCapacity(m.Value)
}

func TB(value uint64) *CapacityMod {

	return &CapacityMod{
		Value: xir.TB(value),
	}

}

func GB(value uint64) *CapacityMod {

	return &CapacityMod{
		Value: xir.GB(value),
	}

}

// ReservedMod is a mod for reserving things: marking them as not for allocation, or non-commissionable
type ReservedMod struct {
	Value uint64
}

func (m *ReservedMod) Modify(x interface{}) {
	x.(xir.SetReserved).SetReserved(m.Value)
}

func Reserved(value uint64) Modifier { return &ReservedMod{Value: value} }

type ConnectorMod struct {
	Value xir.ConnectorKind
}

func (m *ConnectorMod) Modify(x interface{}) {
	x.(xir.SetConnector).SetConnector(m.Value)
}

func RJ45() Modifier {
	return &ConnectorMod{Value: xir.ConnectorKind_RJ45}
}

func QSFPDD() Modifier {
	return &ConnectorMod{Value: xir.ConnectorKind_QSFPDD}
}

func SFP56() Modifier {
	return &ConnectorMod{Value: xir.ConnectorKind_SFP56}
}

func QSFP56() Modifier {
	return &ConnectorMod{Value: xir.ConnectorKind_QSFP56}
}

func SFP28() Modifier {
	return &ConnectorMod{Value: xir.ConnectorKind_SFP28}
}

func QSFP28() Modifier {
	return &ConnectorMod{Value: xir.ConnectorKind_QSFP28}
}

func QSFPP() Modifier {
	return &ConnectorMod{Value: xir.ConnectorKind_QSFPP}
}

func SFPP() Modifier {
	return &ConnectorMod{Value: xir.ConnectorKind_SFPP}
}
