package build

import (
	"fmt"
	//"log"
	//"strings"

	"gitlab.com/mergetb/xir/v0.3/go"
)

func (tb *Builder) Connect(a, b *xir.Port, mods ...Modifier) *xir.Cable {

	id := fmt.Sprintf("%s-%s", a.Label(), b.Label())

	//log.Printf("connecting %s", id)

	c := &xir.Cable{
		Id: id,
		Ends: []*xir.End{
			{Connectors: []*xir.Connector{{Index: 0, Parent: id}}},
			{Connectors: []*xir.Connector{{Index: 1, Parent: id}}},
		},
	}

	for _, mod := range mods {
		mod.Modify(c)
	}

	a.Connector = &xir.Ref{Element: id, Index: 0}
	b.Connector = &xir.Ref{Element: id, Index: 1}

	r := &xir.Ref{Element: a.Parent, Index: a.Index}
	if a.Breakout != nil {
		r.Subref = &xir.Subref{
			Index: a.Breakout.Index,
		}
	}
	c.Ends[0].Connectors[0].Port = r

	r = &xir.Ref{Element: b.Parent, Index: b.Index}
	if b.Breakout != nil {
		r.Subref = &xir.Subref{
			Index: b.Breakout.Index,
		}
	}
	c.Ends[1].Connectors[0].Port = r

	tb.Cables = append(tb.Cables, c)

	return c

}

func (tb *Builder) BreakoutMultiTrunk(
	nic *xir.NIC,
	single *xir.Port,
	multi []*xir.Port,
	bondSingle, bondMulti map[int]string,
	mods ...Modifier,
) []*xir.Cable {

	var cs []*xir.Cable

	breakouts := nic.Breakout(single, multi)

	for i := 0; i < len(breakouts); i++ {

		if bS, ok := bondSingle[i]; ok {
			breakouts[i].Bond = &xir.PortBond{
				Name: bS,
			}
		}

		if bM, ok := bondMulti[i]; ok {
			multi[i].Bond = &xir.PortBond{
				Name: bM,
			}
		}

		c := tb.Connect(breakouts[i], multi[i], mods...)
		cs = append(cs, c)

	}

	return cs

}

func (tb *Builder) BreakoutTrunk(
	nic *xir.NIC,
	single *xir.Port,
	multi []*xir.Port,
	bondSingle, bondMulti string,
	mods ...Modifier,
) []*xir.Cable {

	// one bond on each side
	s := map[int]string{}
	m := map[int]string{}

	for i := 0; i < len(multi); i++ {
		if bondSingle != "" {
			s[i] = bondSingle
		}

		if bondMulti != "" {
			m[i] = bondMulti
		}
	}

	return tb.BreakoutMultiTrunk(
		nic,
		single, multi,
		s, m,
		mods...,
	)

}

func (tb *Builder) Breakout(
	nic *xir.NIC,
	single *xir.Port,
	multi []*xir.Port,
	mods ...Modifier,
) []*xir.Cable {

	return tb.BreakoutTrunk(nic, single, multi, "", "", mods...)

}

func (tb *Builder) Trunk(
	as, bs []*xir.Port,
	bondNameA, bondNameB string,
	mods ...Modifier,
) []*xir.Cable {

	if len(as) != len(bs) {
		panic("trunks must have equal end lengths")
	}

	var cs []*xir.Cable

	for i, a := range as {
		b := bs[i]

		a.Bond = &xir.PortBond{
			Name: bondNameA,
		}
		b.Bond = &xir.PortBond{
			Name: bondNameB,
		}

		cs = append(cs, tb.Connect(a, b, mods...))
	}

	return cs

}

type CableKindMod struct {
	Value xir.CableKind
}

func (m *CableKindMod) Modify(x interface{}) {
	c := x.(*xir.Cable)
	c.Kind = m.Value
}

func DAC() Modifier {
	return &CableKindMod{xir.CableKind_DAC}
}

func DACBreakout() Modifier {
	return &CableKindMod{xir.CableKind_DACBreakout}
}

func AOC() Modifier {
	return &CableKindMod{xir.CableKind_AOC}
}

func AOCBreakout() Modifier {
	return &CableKindMod{xir.CableKind_AOCBreakout}
}

func ACC() Modifier {
	return &CableKindMod{xir.CableKind_ACC}
}

func ACCBreakout() Modifier {
	return &CableKindMod{xir.CableKind_ACCBreakout}
}

type BreakoutConnectorMod struct {
	Single xir.ConnectorKind
	Multi  xir.ConnectorKind
}

func (m *BreakoutConnectorMod) Modify(x interface{}) {
	c := x.(*xir.Cable)
	c.Ends[0].SetConnector(m.Single)
	c.Ends[1].SetConnector(m.Multi)

	// if no CableKind mods are given, set to a generic breakout model
	if c.Kind == xir.CableKind_CableKind_Undefined {
		c.Kind = xir.CableKind_GenericBreakout
	}

}

func QSFPP_4xSFPP() Modifier {
	return &BreakoutConnectorMod{
		Single: xir.ConnectorKind_QSFPP,
		Multi:  xir.ConnectorKind_SFPP,
	}
}

func QSFP28_4xSFP28() Modifier {
	return &BreakoutConnectorMod{
		Single: xir.ConnectorKind_QSFP28,
		Multi:  xir.ConnectorKind_SFP28,
	}
}

func QSFP28_2xQSFP28() Modifier {
	return &BreakoutConnectorMod{
		Single: xir.ConnectorKind_QSFP28,
		Multi:  xir.ConnectorKind_QSFP28,
	}
}

func QSFP56_2xSFP56() Modifier {
	return &BreakoutConnectorMod{
		Single: xir.ConnectorKind_QSFP56,
		Multi:  xir.ConnectorKind_SFP56,
	}
}

func QSFP56_4xSFP56() Modifier {
	return &BreakoutConnectorMod{
		Single: xir.ConnectorKind_QSFP56,
		Multi:  xir.ConnectorKind_SFP56,
	}
}

func QSFP56_2xQSFP56() Modifier {
	return &BreakoutConnectorMod{
		Single: xir.ConnectorKind_QSFP56,
		Multi:  xir.ConnectorKind_QSFP56,
	}
}

func QSFPDD_8xSFP56() Modifier {
	return &BreakoutConnectorMod{
		Single: xir.ConnectorKind_QSFPDD,
		Multi:  xir.ConnectorKind_SFP56,
	}
}

func QSFPDD_8xSFP28() Modifier {
	return &BreakoutConnectorMod{
		Single: xir.ConnectorKind_QSFPDD,
		Multi:  xir.ConnectorKind_SFP28,
	}
}

func QSFPDD_8xLC() Modifier {
	return &BreakoutConnectorMod{
		Single: xir.ConnectorKind_QSFPDD,
		Multi:  xir.ConnectorKind_LC,
	}
}

func QSFPDD_2xQSFP56() Modifier {
	return &BreakoutConnectorMod{
		Single: xir.ConnectorKind_QSFPDD,
		Multi:  xir.ConnectorKind_QSFP56,
	}
}

func QSFPDD_2xQSFP28() Modifier {
	return &BreakoutConnectorMod{
		Single: xir.ConnectorKind_QSFPDD,
		Multi:  xir.ConnectorKind_QSFP28,
	}
}

func QSFPDD_4xQSFP56() Modifier {
	return &BreakoutConnectorMod{
		Single: xir.ConnectorKind_QSFPDD,
		Multi:  xir.ConnectorKind_QSFP56,
	}
}

func QSFPDD_4xQSFP28() Modifier {
	return &BreakoutConnectorMod{
		Single: xir.ConnectorKind_QSFPDD,
		Multi:  xir.ConnectorKind_QSFP28,
	}
}
