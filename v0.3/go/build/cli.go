package build

// TODO(ry) XIR cannot depend on portal/services, this creates a nasty module
// level dependency loop that, while not an import loop, can create huge
// resolution headaches

import (
	"encoding/base64"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"
	"text/tabwriter"

	"github.com/dustin/go-humanize"
	"github.com/spf13/cobra"
	//"gitlab.com/mergetb/portal/services/pkg/commission"
	//"gitlab.com/mergetb/portal/services/pkg/realize/sfe"
	"gitlab.com/mergetb/xir/v0.3/go"
	"google.golang.org/protobuf/encoding/protojson"
)

var tw = tabwriter.NewWriter(os.Stdout, 0, 0, 4, ' ', 0)

func Run(fa *xir.Facility) {

	log.SetFlags(0)

	root := &cobra.Command{
		Use:   fa.Id,
		Short: "Testbed CAD utility",
	}

	cobra.EnablePrefixMatching = true

	var asJSON, genaddrs bool
	save := &cobra.Command{
		Use:   "save",
		Short: "Save the testbed model to XIR",
		Args:  cobra.NoArgs,
		Run:   func(cmd *cobra.Command, args []string) { save(fa, asJSON, genaddrs) },
	}
	save.Flags().BoolVarP(&genaddrs, "genaddr", "g", false, "Generate TPA and infranet addrs")
	save.Flags().BoolVarP(&asJSON, "json", "j", false, "Save as JSON")
	root.AddCommand(save)

	list := &cobra.Command{
		Use:   "list",
		Short: "List items in the model",
		Args:  cobra.NoArgs,
	}
	root.AddCommand(list)

	listAll := &cobra.Command{
		Use:   "all",
		Short: "List nodes, swiches and infrastructure servers",
		Args:  cobra.NoArgs,
		Run:   func(cmd *cobra.Command, args []string) { listAll(fa) },
	}
	list.AddCommand(listAll)

	listNodes := &cobra.Command{
		Use:   "nodes",
		Short: "List nodes",
		Args:  cobra.NoArgs,
		Run:   func(cmd *cobra.Command, args []string) { listNodes(fa) },
	}
	list.AddCommand(listNodes)

	listSwitches := &cobra.Command{
		Use:   "switches",
		Short: "List switches",
		Args:  cobra.NoArgs,
		Run:   func(cmd *cobra.Command, args []string) { listSwitches(fa) }}
	list.AddCommand(listSwitches)

	listInfra := &cobra.Command{
		Use:   "infrastructure",
		Short: "List infrastructure",
		Args:  cobra.NoArgs,
		Run:   func(cmd *cobra.Command, args []string) { listInfrastructure(fa) },
	}
	list.AddCommand(listInfra)

	listNeighbors := &cobra.Command{
		Use:   "neighbors",
		Short: "List a resource's neighbors",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			listNeighbors(fa, args[0])
		},
	}
	list.AddCommand(listNeighbors)

	spec := &cobra.Command{
		Use:   "spec",
		Short: "Generate specs",
	}
	root.AddCommand(spec)

	cablingSpec := &cobra.Command{
		Use:   "cabling",
		Short: "Generate a cabling spec",
		Args:  cobra.NoArgs,
		Run:   func(*cobra.Command, []string) { cablingSpec(fa) },
	}
	spec.AddCommand(cablingSpec)

	graphologySpec := &cobra.Command{
		Use:   "graphology",
		Short: "Generate a graphology spec",
		Args:  cobra.NoArgs,
		Run:   func(*cobra.Command, []string) { saveGraphologySpec(fa) },
	}
	spec.AddCommand(graphologySpec)

	serve := &cobra.Command{
		Use:   "server",
		Short: "Run model server",
		Args:  cobra.NoArgs,
		Run:   func(*cobra.Command, []string) { modelServer(fa) },
	}
	root.AddCommand(serve)

	lom := &cobra.Command{
		Use:   "lom",
		Short: "List of materials",
		Args:  cobra.NoArgs,
		Run:   func(*cobra.Command, []string) { listOfMaterials(fa) },
	}
	root.AddCommand(lom)

	/*
		var (
			itpa uint64
			xtpa uint64
		)
		routes := &cobra.Command{
			Use:   "routes",
			Short: "Show testbed routing table",
			Args:  cobra.NoArgs,
			Run:   func(*cobra.Command, []string) { showRoutes(itpa, xtpa, fa.Lift()) },
		}
		routes.Flags().Uint64VarP(
			&itpa,
			"infranet-tpa",
			"i",
			0x004D000000000000,
			"Infranet base TPA Address",
		)
		routes.Flags().Uint64VarP(
			&xtpa,
			"xpnet-tpa",
			"x",
			0x004E000000000000,
			"Experiment network base TPA Address",
		)
		root.AddCommand(routes)
	*/

	/*
		addrs := &cobra.Command{
			Use:   "addrs",
			Short: "Assign and show infranet addrs based on empty resource space",
			Args:  cobra.NoArgs,
			Run:   func(*cobra.Command, []string) { showAddrs(fa.Lift()) },
		}
		root.AddCommand(addrs)
	*/

	root.Execute()

}

/*
func showAddrs(tbx *xir.Topology) {

	bp, err := commission.NewBlockPool("172.30.0.0", "172.30.255.192")
	if err != nil {
		log.Fatalf("create block pool: %v", err)
	}

	err = commission.AssignInfranetAddrs(tbx, bp)
	if err != nil {
		log.Fatalf("assign addrs: %v", err)
	}

	for _, x := range tbx.Devices {
		r := x.Resource()
		if r == nil {
			continue
		}

		if r.LeafConfig != nil {
			fmt.Println(r.Id)
			fmt.Printf("%+v\n", r.LeafConfig)
		}

		if len(r.InfranetAddr) != 0 {
			fmt.Println(r.Id)
			for vrf, addrs := range r.InfranetAddr {
				fmt.Printf("%s:\n", vrf)
				for _, addr := range addrs.List {
					fmt.Printf("%+v\n", addr)
				}
			}
		}
	}

}
*/

/*
func showRoutes(itpa, xtpa uint64, tbx *xir.Topology) {

	lm, nm := sfe.AssignTPAsXpNet(sfe.TPA(xtpa), tbx)
	fmt.Println("XPNET")
	fmt.Println("=====")
	fmt.Println("SWITCH")
	for k, v := range lm {
		fmt.Printf("%4s: %s\n", k.Id(), v)
	}

	fmt.Println("NODE")
	for k, v := range nm {
		fmt.Printf("%4s.%d: %s\n", k.Device.Id(), k.Ref().Index, v)
	}

	ilm, inm := sfe.AssignTPAsInfraNet(sfe.TPA(itpa), tbx)
	fmt.Println("INFRANET")
	fmt.Println("========")
	fmt.Println("SWITCH")
	for k, v := range ilm {
		fmt.Printf("%4s: %s\n", k.Id(), v)
	}

	fmt.Println("NODE")
	for k, v := range inm {
		fmt.Printf("%4s.%d: %s\n", k.Device.Id(), k.Ref().Index, v)
	}

	xrt := sfe.BuildXpRoutingTable(tbx)

	fmt.Println("XP ROUTES")
	fmt.Println("=========")
	for node, routes := range xrt {

		fmt.Println(node)
		for _, route := range routes {
			fmt.Printf("%s\n", route)
		}
	}

	irt := sfe.BuildInfraRoutingTable(tbx)

	fmt.Println("")
	fmt.Println("INFRA ROUTES")
	fmt.Println("============")
	for node, routes := range irt {

		fmt.Println(node)
		for _, route := range routes {
			fmt.Printf("%s\n", route)
		}
	}

}
*/

func save(fa *xir.Facility, asJSON, genaddrs bool) {

	var out []byte
	var ext string
	var err error

	/*
		if genaddrs {
			tbt := fa.Lift()
			bp, err := commission.NewBlockPool("172.30.0.0", "172.30.255.192")
			if err != nil {
				log.Fatalf("create block pool: %v", err)
			}

			err = commission.AssignInfranetAddrs(tbt, bp)
			if err != nil {
				log.Fatalf("assign addrs: %v", err)
			}
		}
	*/

	if asJSON {
		opts := protojson.MarshalOptions{
			Multiline: true,
			Indent:    "  ",
		}
		out, err = opts.Marshal(fa)
		if err != nil {
			log.Fatalf("facility to JSON: %v", err)
		}
		ext = "json"
	} else {
		buf, err := fa.ToBuf()
		if err != nil {
			log.Fatalf("facility to protobuf: %v", err)
		}
		out = []byte(base64.StdEncoding.EncodeToString(buf))
		ext = "xir"
	}

	err = ioutil.WriteFile(
		fmt.Sprintf("%s.%s", fa.Id, ext), []byte(out), 0644)
	if err != nil {
		log.Fatal(err)
	}

}

func listAll(fa *xir.Facility) {

	fmt.Println("Nodes")
	fmt.Println("-----")
	listNodes(fa)
	fmt.Println("")

	fmt.Println("Switches")
	fmt.Println("--------")
	listSwitches(fa)
	fmt.Println("")

	fmt.Println("Infrastructure")
	fmt.Println("--------------")
	listInfrastructure(fa)

}

func listNodes(fa *xir.Facility) {

	fmt.Fprintf(tw, "ID\tCORES (RESERVED)\tMEMORY (RESERVED)\tSTORAGE\tXPNET\tINFRANET\tALLOC\n")

	count := 0
	for _, r := range fa.Resources {

		if !r.HasRole(xir.Role_TbNode) {
			continue
		}

		var alloc []string
		for _, x := range r.Alloc {
			alloc = append(alloc, x.String())
		}

		fmt.Fprintf(tw, "%s\t%d (%d)\t%s (%s)\t%s\t%s\t%s\t%s\n",
			r.Id,
			r.TotalCores(),
			r.ReservedCores(),
			humanize.IBytes(r.TotalMem()),
			humanize.IBytes(r.ReservedMem()),
			humanize.IBytes(r.Storage()),
			humanize.SI(float64(r.XpBandwidth()), "bps"),
			humanize.SI(float64(r.InfraBandwidth()), "bps"),
			strings.Join(alloc, " "),
		)
		count++

	}
	tw.Flush()
	log.Printf("---------")
	log.Printf("COUNT: %d", count)

}

func listInfrastructure(fa *xir.Facility) {

	fmt.Fprintf(tw, "ID\tCORES (RESERVED)\tMEMORY (RESERVED)\tSTORAGE\tXPNET\tINFRANET\tALLOC\n")

	count := 0
	for _, r := range fa.Resources {

		if r.HasRole(xir.Role_TbNode, xir.Role_InfraSwitch, xir.Role_XpSwitch) {
			continue
		}

		var alloc []string
		for _, x := range r.Alloc {
			alloc = append(alloc, x.String())
		}

		fmt.Fprintf(tw, "%s\t%d (%d)\t%s (%s)\t%s\t%s\t%s\t%s\n",
			r.Id,
			r.Cores(),
			r.ReservedCores(),
			humanize.IBytes(r.TotalMem()),
			humanize.IBytes(r.ReservedMem()),
			humanize.IBytes(r.Storage()),
			humanize.SI(float64(r.XpBandwidth()), "bps"),
			humanize.SI(float64(r.InfraBandwidth()), "bps"),
			strings.Join(alloc, " "),
		)
		count++

	}
	tw.Flush()
	log.Printf("---------")
	log.Printf("COUNT: %d", count)

}

func listSwitches(fa *xir.Facility) {

	fmt.Fprintf(tw, "ID\tSWPS\tBANDWIDTH\tROLE\n")

	count := 0
	for _, r := range fa.Resources {

		if !r.HasRole(
			xir.Role_Gateway,
			xir.Role_InfraSwitch,
			xir.Role_XpSwitch,
			xir.Role_MgmtSwitch,
		) {
			continue
		}

		var role []string
		for _, x := range r.Roles {
			role = append(role, x.String())
		}

		fmt.Fprintf(tw, "%s\t%d\t%s\t%s\n",
			r.Id,
			len(r.Swps()),
			humanize.SI(float64(r.SwpBandwidth()), "bps"),
			strings.Join(role, " "),
		)

		count++

	}
	tw.Flush()
	log.Printf("---------")
	log.Printf("COUNT: %d", count)

}

func listNeighbors(fa *xir.Facility, id string) {

	topo := fa.Lift()

	d := topo.Device(id)
	if d == nil {
		log.Fatalf("resource %s not found", id)
	}

	_listNeighbors(d, "", false)
}

func _listNeighbors(d *xir.Device, indent string, showCable bool) {

	for _, ifx := range d.Interfaces {
		if ifx.Edge == nil || ifx.Edge.Connection == nil {
			continue
		}
		c0 := ifx.Edge.Connector()
		for _, e := range ifx.Edge.Connection.Edges {

			if e == ifx.Edge {
				continue
			}
			c1 := e.Connector()
			if c0.Index > 0 && c1.Index > 0 {
				// both edges part of a breakout
				continue
			}
			if e.Interface == nil || e.Interface.Device == nil {
				continue
			}
			if e.Interface.Device == d {
				continue
			}

			cable := ""
			if showCable {
				pi := ifx.Edge.Connection.Cable().ProductInfo
				if pi != nil && pi.Model != "" {
					cable = " (" + pi.Manufacturer + " " + pi.Model + " " + pi.SKU + ")"
				}
			}

			log.Printf("%s%s: %s.%s%s",
				indent,
				ifx.PhysPortName(),
				e.Interface.Device.Id(),
				e.Interface.PhysPortName(),
				cable,
			)

		}
	}

}
