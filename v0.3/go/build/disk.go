package build

import (
	"gitlab.com/mergetb/xir/v0.3/go"
)

type DiskConstructor func() *xir.Disk

func (ctor DiskConstructor) Modify(x interface{}) {
	r := x.(*xir.Resource)
	r.Disks = append(r.Disks, ctor())
}

type DisksConstructor func() []*xir.Disk

func (ctor DisksConstructor) Modify(x interface{}) {
	r := x.(*xir.Resource)
	r.Disks = append(r.Disks, ctor()...)
}

type DiskInterfaceMod struct {
	Value xir.DiskInterface
}

func (m *DiskInterfaceMod) Modify(x interface{}) {
	d := x.(*xir.Disk)
	d.DiskInterface = m.Value
}

func Virtio() Modifier {
	return &DiskInterfaceMod{Value: xir.DiskInterface_VirtioBlock}
}

func Sata3() Modifier {
	return &DiskInterfaceMod{Value: xir.DiskInterface_SATA3}
}

type DiskRoleMod struct {
	Value xir.DiskRole
}

func (m *DiskRoleMod) Modify(x interface{}) {
	d := x.(*xir.Disk)
	d.Roles = append(d.Roles, m.Value)
}

func SysDisk() Modifier {
	return &DiskRoleMod{Value: xir.DiskRole_System}
}

func EtcdDisk() Modifier {
	return &DiskRoleMod{Value: xir.DiskRole_Etcd}
}

func MinioDisk() Modifier {
	return &DiskRoleMod{Value: xir.DiskRole_MinIO}
}

func RallyDisk() Modifier {
	return &DiskRoleMod{Value: xir.DiskRole_Rally}
}

func MarinerDisk() Modifier {
	return &DiskRoleMod{Value: xir.DiskRole_Mariner}
}

func SSDs(count uint, mods ...Modifier) Modifier {

	return DisksConstructor(func() []*xir.Disk {
		var ds []*xir.Disk

		for i := uint(0); i < count; i++ {
			d := &xir.Disk{
				FormFactor: xir.DiskFormFactor_SSD25,
			}
			ds = append(ds, d)
			for _, mod := range mods {
				mod.Modify(d)
			}
		}

		return ds
	})

}

// "count" NVMEs on controller "controllerIndex"
func NVMEsController(count, controllerIndex uint, mods ...Modifier) Modifier {
	m := make(map[uint]uint)
	for i := uint(0); i < count; i++ {
		m[i] = controllerIndex
	}

	return nvmeControllers(count, m, mods...)
}

// "count" NVME controllers with 1 device each
func NVMEs(count, controllerIndex uint, mods ...Modifier) Modifier {
	m := make(map[uint]uint)
	for i := uint(0); i < count; i++ {
		m[i] = controllerIndex + i
	}

	return nvmeControllers(count, m, mods...)
}

func nvmeControllers(count uint, controllers map[uint]uint, mods ...Modifier) Modifier {

	return DisksConstructor(func() []*xir.Disk {
		var ds []*xir.Disk

		for i := uint(0); i < count; i++ {
			d := &xir.Disk{
				FormFactor:          xir.DiskFormFactor_SSD25,
				DiskInterface:       xir.DiskInterface_NVMEx4,
				NVMEControllerIndex: uint32(controllers[i]),
			}
			ds = append(ds, d)
			for _, mod := range mods {
				mod.Modify(d)
			}
		}

		return ds
	})

}

func HDDs(count uint, mods ...Modifier) Modifier {

	return DisksConstructor(func() []*xir.Disk {
		var ds []*xir.Disk

		for i := uint(0); i < count; i++ {
			d := &xir.Disk{
				FormFactor: xir.DiskFormFactor_HDD35,
			}
			ds = append(ds, d)
			for _, mod := range mods {
				mod.Modify(d)
			}
		}

		return ds
	})

}

func Disks(count uint, mods ...Modifier) Modifier {

	return DisksConstructor(func() []*xir.Disk {
		var ds []*xir.Disk

		for i := uint(0); i < count; i++ {
			d := &xir.Disk{
				FormFactor: xir.DiskFormFactor_SSD25,
			}
			ds = append(ds, d)
			for _, mod := range mods {
				mod.Modify(d)
			}
		}

		return ds
	})

}
