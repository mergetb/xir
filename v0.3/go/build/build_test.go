package build

import (
	"testing"
)

// GetTestBuild returns a Builder preconfigured for a
// common setup that can be used in testing
func getTestBuild() (*Builder, error) {
	tb, err := NewBuilder(
		"test",
		"test.mergetb.net",
		"",
	)
	if err != nil {
		return nil, err
	}
	tb.Node("test-node",
		Procs(2, Cores(8)),
		Dimms(4, GB(8)),
		Ens(2, PortNames("ens1f0", "ens1f1"), VFs(2), Gbps(10), Xpnet()),
	)
	//add more so we can test in different places
	return tb, nil
}

// TestVFNameType tests naming conventions of VF interfaces match
// `biosdevname` convention ("pfname_vfindex")
func TestVFNameType(t *testing.T) {
	tb, err := getTestBuild()
	if err != nil {
		t.Fatalf("Error creating a testbed (%v)", err)
	}
	expectedVFNames := []string{
		"ens1f0", "ens1f1", "ens1f0_0", "ens1f0_1", "ens1f1_0", "ens1f1_1",
	}
	expectedSriovTypes := []string{
		"PF", "PF", "VF", "VF", "VF", "VF",
	}

	r := tb.Resources[0]
	for pi, p := range r.NICs[0].Ports {
		ename := expectedVFNames[int32(pi)]
		etype := expectedSriovTypes[int32(pi)]
		aname := r.PortName(p)
		atype := "??"
		if p.Sriov.GetVf() != nil {
			atype = "VF"
		} else if p.Sriov.GetPf() != nil {
			atype = "PF"
		}
		t.Logf("resource: %v \t %v \t %s", pi, aname, atype)
		if aname != ename {
			t.Fatalf("Port Name didn't match: expected %s, actual %s", ename, aname)
		}
		if atype != etype {
			t.Fatalf("Port Sriov Type didn't match: expected %s, actual %s", etype, atype)
		}
	}
}
