package build

import (
	proto "github.com/golang/protobuf/proto"
	"gitlab.com/mergetb/xir/v0.3/go"
	"log"
)

type NICConstructor func() *xir.NIC

func (ctor NICConstructor) Modify(x interface{}) {

	r := x.(*xir.Resource)

	nic := ctor()

	idx := uint32(0)

	// watch out for breakouts
	for _, n := range r.NICs {
		nports := uint32(0)
		for _, p := range n.Ports {
			if p.Breakout == nil || p.Breakout.Index == p.Breakout.Radix-1 {
				nports += 1
			}
		}

		idx += nports
	}

	for _, p := range nic.Ports {
		p.Parent = r.Id
		p.Index = idx
		idx++
	}

	ni := len(r.NICs) // we are going to append nic to r.NICs, its index will be ni
	r.NICs = append(r.NICs, nic)

	// sriov/vf pass: now that all port's modifiers have been applied,
	// fill in missing values for SRIOV
	for pi, p := range nic.Ports {
		if p.Sriov == nil {
			continue
		}
		p.Sriov.PfNi = uint32(ni)
		if p.Sriov.GetVf() == nil {
			p.Sriov.PfPi = uint32(pi) // for PF ports, indexes are their own's
			continue
		}
		// vf-case
		pfport := r.GetPf(p)
		if pfport == nil {
			log.Fatalf("Can't determine PF for port %+v", p)
		}
		// now we're going to replace p with a clone of pfport
		vfp := proto.Clone(pfport).(*xir.Port)
		// finally reuse p's values, and replace p on nic.Ports
		vfp.Name = p.Name
		vfp.Mac = p.Mac
		vfp.Sriov = p.Sriov
		vfp.Index = p.Index

		nic.Ports[pi] = vfp
	}

}

// xxx shouldn't this be called nic_ctor(...)?
func port(
	count uint,
	startingIndex uint32,
	kind xir.NICKind,
	mods ...Modifier,
) Modifier {

	return NICConstructor(func() *xir.NIC {

		nic := &xir.NIC{
			Kind:          kind,
			StartingIndex: startingIndex,
		}

		for i := uint(0); i < count; i++ {
			nic.Ports = append(nic.Ports, new(xir.Port))
		}

		for _, mod := range mods {
			mod.Modify(nic)
		}

		return nic
	})

}

func Eth(count uint, mods ...Modifier) Modifier {

	return port(count, 0, xir.NICKind_ETH, mods...)

}

func Swp(count uint, mods ...Modifier) Modifier {

	return port(count, 1, xir.NICKind_SWP, mods...)

}

func Eno(count uint, mods ...Modifier) Modifier {

	return port(count, 1, xir.NICKind_ENO, mods...)

}

func Enp(count uint, start uint32, mods ...Modifier) Modifier {

	return port(count, start, xir.NICKind_ENP, mods...)

}

func Ens(count uint, mods ...Modifier) Modifier {

	return port(count, 1, xir.NICKind_ENS, mods...)

}

func Ipmi(count uint, mods ...Modifier) Modifier {

	return port(count, 0, xir.NICKind_IPMI, mods...)

}

func Combo(count uint, mods ...Modifier) Modifier {

	return port(count, 0, xir.NICKind_Combo, mods...)

}

func Gbps(value uint64) Modifier {

	return &CapacityMod{Value: xir.Gbps(value)}

}

func Mbps(value uint64) Modifier {

	return &CapacityMod{Value: xir.Mbps(value)}

}

type PortNameMod struct {
	Names []string
}

func (m *PortNameMod) Modify(nic interface{}) {
	n := nic.(*xir.NIC)
	for pi, p := range n.Ports {
		if pi < len(m.Names) {
			p.Name = m.Names[pi]
		}
	}
}

func PortNames(names ...string) Modifier {
	return &PortNameMod{Names: names}
}

// VFMod is a Modifier for marking this port as as supporting Value VFs
type VFCountMod struct {
	VFCount uint32
}

// Modify appends a given number of VF-ports to the NIC (for each existing port)
func (m *VFCountMod) Modify(nic interface{}) {
	n := nic.(*xir.NIC)
	for pfpi, p := range n.Ports {
		p.SriovSetPF(0, uint32(pfpi), m.VFCount)
		// create VF ports on this interface
		for i := 0; i < int(m.VFCount); i++ {
			// temporary element for a vf-port, all fields will be set
			// later in NICConstructor.Modify.  This is done to ensure that
			// all modifiers are applied before we copy their values
			vfp := &xir.Port{}
			// here we only know PfPi, PfNi will be set in  NICConstructor.Modify
			vfp.SriovSetVF(0, uint32(pfpi), uint32(i))
			n.Ports = append(n.Ports, vfp)
		}
	}
}

// VFs returns a modifier that sets count of VFs to a given integer for all ports of the nic
func VFs(vfcnt uint32) Modifier {
	return &VFCountMod{VFCount: vfcnt}
}

// LinkRoleMod is a Modifier for changing LinkRole
type LinkRoleMod struct {
	Value   xir.LinkRole
	Indices []uint
}

func (m *LinkRoleMod) Modify(nic interface{}) {

	if len(m.Indices) == 0 {
		for _, p := range nic.(*xir.NIC).Ports {
			p.Role = m.Value
		}
	} else {
		for _, i := range m.Indices {
			nic.(*xir.NIC).Ports[i].Role = m.Value
		}
	}

}

func Xpnet(indices ...uint) Modifier {
	return &LinkRoleMod{Value: xir.LinkRole_XpLink, Indices: indices}
}

func Infranet(indices ...uint) Modifier {
	return &LinkRoleMod{Value: xir.LinkRole_InfraLink, Indices: indices}
}

func Mgmt(indices ...uint) Modifier {
	return &LinkRoleMod{Value: xir.LinkRole_MgmtLink, Indices: indices}
}

func Gw(indices ...uint) Modifier {
	return &LinkRoleMod{Value: xir.LinkRole_GatewayLink, Indices: indices}
}

func Emu(indices ...uint) Modifier {
	return &LinkRoleMod{Value: xir.LinkRole_EmuLink, Indices: indices}
}
