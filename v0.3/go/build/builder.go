package build

import (
	"fmt"
	"os"

	"gitlab.com/mergetb/tech/shared/install/cache"
	xir "gitlab.com/mergetb/xir/v0.3/go"
)

var (
	IndexPad = 0
)

type underlay struct {
	subnet string
	// counters
	ip  *IPCounter
	asn uint32
}

func NewUnderlay(subnet string, asnbegin uint32) (*underlay, error) {
	c, err := NewIPCounter(subnet)
	if err != nil {
		return nil, err
	}

	u := &underlay{
		subnet: subnet,
		ip:     c,
		asn:    asnbegin,
	}

	return u, nil
}

func (u *underlay) IncBGP() {
	u.asn++
	u.ip.Inc()
}

type Builder struct {
	Id        string
	FQDN      string
	Underlay  map[string]*underlay
	Resources []*xir.Resource
	Cables    []*xir.Cable
	Cache     *cache.Cache
}

func NewBuilder(id, fqdn string) (*Builder, error) {

	infra, err := NewUnderlay(
		"10.99.0.0/24",
		4200000000,
	)
	if err != nil {
		return nil, err
	}

	xp, err := NewUnderlay(
		"10.99.1.0/24",
		4210000000,
	)
	if err != nil {
		return nil, err
	}

	cachefile := os.Getenv("BUILDER_CACHE")
	if cachefile == "" {
		cachefile = "cache.json"
	}

	cache, err := cache.NewCache(cachefile)
	if err != nil {
		return nil, err
	}

	return &Builder{
		Id:   id,
		FQDN: fqdn,
		Underlay: map[string]*underlay{
			"ifabric": infra,
			"xfabric": xp,
		},
		Cache: cache,
	}, nil

}

func (b *Builder) Gateways(
	count int, idprefix string, mods ...Modifier) []*xir.Resource {

	return resources(b.Gateway, count, idprefix, mods...)

}

func (b *Builder) getBGP(id string, has_infra, has_xp bool) (ret []*xir.BGPRouterConfig) {
	n := b.Cache.EnsureNode(id)

	vrfs := []string{}

	if has_infra {
		vrfs = append(vrfs, "ifabric")
	}
	if has_xp {
		vrfs = append(vrfs, "xfabric")
	}

	for _, vrf := range vrfs {

		if _, ok := n.BGP[vrf]; !ok {

			// find the next unallocated ASN
			u := b.Underlay[vrf]
			for {
				if !b.Cache.IsAllocatedASN(vrf, u.asn) {
					break
				}
				u.IncBGP()
			}

			n.BGP[vrf] = &cache.BGP{
				ASN: u.asn,
				IP:  u.ip.String(),
			}
		}

		ret = append(ret, &xir.BGPRouterConfig{
			Vrf: vrf,
			ASN: n.BGP[vrf].ASN,
			Interfaces: []*xir.InterfaceConfig{
				{
					Address:   n.BGP[vrf].IP,
					Interface: "lo",
				},
			},
		})
	}

	return
}

func (b *Builder) Gateway(id string, mods ...Modifier) *xir.Resource {

	r := &xir.Resource{
		Id:    id,
		Alloc: []xir.AllocMode{xir.AllocMode_Net},
		Roles: []xir.Role{xir.Role_Gateway},
		OS: &xir.OSConfig{
			BGP: b.getBGP(id, true, true),
		},
	}

	for _, mod := range mods {
		mod.Modify(r)
	}

	b.Resources = append(b.Resources, r)

	return r
}

func (b *Builder) InfraSpines(
	count int, idprefix string, mods ...Modifier) []*xir.Resource {

	return resources(b.InfraSpine, count, idprefix, mods...)

}

func (b *Builder) InfraSpine(id string, mods ...Modifier) *xir.Resource {

	r := &xir.Resource{
		Id:    id,
		Alloc: []xir.AllocMode{xir.AllocMode_Net},
		Roles: []xir.Role{
			xir.Role_InfraSwitch,
			xir.Role_Spine,
		},
		OS: &xir.OSConfig{
			BGP: b.getBGP(id, true, false),
		},
	}

	for _, mod := range mods {
		mod.Modify(r)
	}

	b.Resources = append(b.Resources, r)

	return r
}

func (b *Builder) InfraLeaves(
	count int, idprefix string, mods ...Modifier) []*xir.Resource {

	return resources(b.InfraLeaf, count, idprefix, mods...)

}

func (b *Builder) InfraLeaf(id string, mods ...Modifier) *xir.Resource {

	r := &xir.Resource{
		Id:    id,
		Alloc: []xir.AllocMode{xir.AllocMode_Net},
		Roles: []xir.Role{
			xir.Role_InfraSwitch,
			xir.Role_Leaf,
		},
		OS: &xir.OSConfig{
			BGP: b.getBGP(id, true, false),
		},
	}

	for _, mod := range mods {
		mod.Modify(r)
	}

	b.Resources = append(b.Resources, r)

	return r
}

func (b *Builder) InfraStems(
	count int, idprefix string, mods ...Modifier) []*xir.Resource {

	return resources(b.InfraStem, count, idprefix, mods...)

}

func (b *Builder) InfraStem(id string, mods ...Modifier) *xir.Resource {

	r := &xir.Resource{
		Id:    id,
		Alloc: []xir.AllocMode{xir.AllocMode_Net},
		Roles: []xir.Role{
			xir.Role_InfraSwitch,
			xir.Role_Stem,
		},
		OS: &xir.OSConfig{
			BGP: b.getBGP(id, true, false),
		},
	}

	for _, mod := range mods {
		mod.Modify(r)
	}

	b.Resources = append(b.Resources, r)

	return r
}

func (b *Builder) XpSpines(
	count int, idprefix string, mods ...Modifier) []*xir.Resource {

	return resources(b.XpSpine, count, idprefix, mods...)

}

func (b *Builder) XpSpine(id string, mods ...Modifier) *xir.Resource {

	r := &xir.Resource{
		Id:    id,
		Alloc: []xir.AllocMode{xir.AllocMode_Net},
		Roles: []xir.Role{
			xir.Role_XpSwitch,
			xir.Role_Spine,
		},
		OS: &xir.OSConfig{
			BGP: b.getBGP(id, false, true),
		},
	}

	for _, mod := range mods {
		mod.Modify(r)
	}

	b.Resources = append(b.Resources, r)

	return r
}

func (b *Builder) XpStems(
	count int, idprefix string, mods ...Modifier) []*xir.Resource {

	return resources(b.XpStem, count, idprefix, mods...)

}

func (b *Builder) XpStem(id string, mods ...Modifier) *xir.Resource {

	r := &xir.Resource{
		Id:    id,
		Alloc: []xir.AllocMode{xir.AllocMode_Net},
		Roles: []xir.Role{
			xir.Role_XpSwitch,
			xir.Role_Stem,
		},
		OS: &xir.OSConfig{
			BGP: b.getBGP(id, false, true),
		},
	}

	for _, mod := range mods {
		mod.Modify(r)
	}

	b.Resources = append(b.Resources, r)

	return r
}

func (b *Builder) XpLeaves(
	count int, idprefix string, mods ...Modifier) []*xir.Resource {

	return resources(b.XpLeaf, count, idprefix, mods...)

}

func (b *Builder) XpLeaf(id string, mods ...Modifier) *xir.Resource {

	r := &xir.Resource{
		Id:    id,
		Alloc: []xir.AllocMode{xir.AllocMode_Net},
		Roles: []xir.Role{
			xir.Role_XpSwitch,
			xir.Role_Leaf,
		},
		OS: &xir.OSConfig{
			BGP: b.getBGP(id, false, true),
		},
	}

	for _, mod := range mods {
		mod.Modify(r)
	}

	b.Resources = append(b.Resources, r)

	return r
}

func (b *Builder) MgmtSpines(
	count int, idprefix string, mods ...Modifier) []*xir.Resource {

	return resources(b.MgmtSpine, count, idprefix, mods...)

}

func (b *Builder) MgmtSpine(id string, mods ...Modifier) *xir.Resource {

	r := &xir.Resource{
		Id:    id,
		Alloc: []xir.AllocMode{xir.AllocMode_Net},
		Roles: []xir.Role{
			xir.Role_MgmtSwitch,
			xir.Role_Spine,
		},
	}

	for _, mod := range mods {
		mod.Modify(r)
	}

	b.Resources = append(b.Resources, r)

	return r
}

func (b *Builder) MgmtLeaves(
	count int, idprefix string, mods ...Modifier) []*xir.Resource {

	return resources(b.MgmtLeaf, count, idprefix, mods...)

}

func (b *Builder) MgmtLeaf(id string, mods ...Modifier) *xir.Resource {

	r := &xir.Resource{
		Id:    id,
		Alloc: []xir.AllocMode{xir.AllocMode_Net},
		Roles: []xir.Role{
			xir.Role_MgmtSwitch,
			xir.Role_Leaf,
		},
	}

	for _, mod := range mods {
		mod.Modify(r)
	}

	b.Resources = append(b.Resources, r)

	return r
}

func (b *Builder) OpsServers(
	count int, idprefix string, mods ...Modifier) []*xir.Resource {

	return resources(b.OpsServer, count, idprefix, mods...)

}

func (b *Builder) OpsServer(id string, mods ...Modifier) *xir.Resource {

	r := &xir.Resource{
		Id:    id,
		Alloc: []xir.AllocMode{xir.AllocMode_NoAlloc},
		Roles: []xir.Role{xir.Role_OpsServer},
	}

	for _, mod := range mods {
		mod.Modify(r)
	}

	b.Resources = append(b.Resources, r)

	return r
}

func (b *Builder) Infraservers(
	count int, idprefix string, mods ...Modifier) []*xir.Resource {

	return resources(b.Infraserver, count, idprefix, mods...)

}

func (b *Builder) Infraserver(id string, mods ...Modifier) *xir.Resource {

	r := &xir.Resource{
		Id:    id,
		Alloc: []xir.AllocMode{xir.AllocMode_NoAlloc},
		Roles: []xir.Role{
			xir.Role_InfraServer,
			xir.Role_EtcdHost,
			xir.Role_MinIOHost,
			xir.Role_RexHost,
			xir.Role_DriverHost,
			xir.Role_ManagerHost,
			xir.Role_CommanderHost,
			xir.Role_InfrapodServer,
		},
		OS: &xir.OSConfig{
			BGP: b.getBGP(id, true, false),
		},
	}

	for _, mod := range mods {
		mod.Modify(r)
	}

	b.Resources = append(b.Resources, r)

	return r
}

func (b *Builder) InfrapodServers(
	count int, idprefix string, mods ...Modifier) []*xir.Resource {

	return resources(b.InfrapodServer, count, idprefix, mods...)

}

func (b *Builder) InfrapodServer(id string, mods ...Modifier) *xir.Resource {

	r := &xir.Resource{
		Id:    id,
		Alloc: []xir.AllocMode{xir.AllocMode_NoAlloc},
		Roles: []xir.Role{
			xir.Role_InfrapodServer,
		},
		OS: &xir.OSConfig{
			BGP: b.getBGP(id, true, false),
		},
	}

	for _, mod := range mods {
		mod.Modify(r)
	}

	b.Resources = append(b.Resources, r)

	return r
}
func (b *Builder) StorageServers(
	count int, idprefix string, mods ...Modifier) []*xir.Resource {

	return resources(b.StorageServer, count, idprefix, mods...)

}

func (b *Builder) StorageServer(id string, mods ...Modifier) *xir.Resource {

	r := &xir.Resource{
		Id: id,
		Alloc: []xir.AllocMode{
			xir.AllocMode_Filesystem,
			xir.AllocMode_BlockDevice,
		},
		Roles: []xir.Role{
			xir.Role_StorageServer,
			xir.Role_SledHost,
		},
		OS: &xir.OSConfig{
			BGP: b.getBGP(id, true, false),
		},
	}

	for _, mod := range mods {
		mod.Modify(r)
	}

	b.Resources = append(b.Resources, r)

	return r

}

func (b *Builder) NetworkEmulators(
	count int, idprefix string, mods ...Modifier) []*xir.Resource {

	return resources(b.NetworkEmulator, count, idprefix, mods...)

}

func (b *Builder) NetworkEmulator(id string, mods ...Modifier) *xir.Resource {

	r := &xir.Resource{
		Id: id,
		Alloc: []xir.AllocMode{
			xir.AllocMode_NetEmu,
		},
		Roles: []xir.Role{
			xir.Role_NetworkEmulator,
		},
		OS: &xir.OSConfig{
			BGP: b.getBGP(id, false, true),
		},
	}

	for _, mod := range mods {
		mod.Modify(r)
	}

	b.Resources = append(b.Resources, r)

	return r

}

func (b *Builder) PhysicsSimulators(
	count int, idprefix string, mods ...Modifier) []*xir.Resource {

	return resources(b.PhysicsSimulator, count, idprefix, mods...)

}

func (b *Builder) PhysicsSimulator(id string, mods ...Modifier) *xir.Resource {

	r := &xir.Resource{
		Id: id,
		Alloc: []xir.AllocMode{
			xir.AllocMode_Physim,
		},
		Roles: []xir.Role{
			xir.Role_PhysicsSimulator,
		},
		OS: &xir.OSConfig{
			BGP: b.getBGP(id, false, true),
		},
	}

	for _, mod := range mods {
		mod.Modify(r)
	}

	b.Resources = append(b.Resources, r)

	return r

}

func (b *Builder) Nodes(
	count int, idprefix string, mods ...Modifier) []*xir.Resource {

	return resources(b.Node, count, idprefix, mods...)

}

func (b *Builder) Node(id string, mods ...Modifier) *xir.Resource {

	r := &xir.Resource{
		Id:    id,
		Alloc: []xir.AllocMode{},
		Roles: []xir.Role{
			xir.Role_TbNode,
		},
	}

	for _, mod := range mods {
		mod.Modify(r)
	}

	//xxx hack: force hard-coded VF names
	for _, n := range r.NICs {
		for _, p := range n.Ports {
			if p.Name == "" {
				p.Name = r.PortName(p)
			}
		}
	}
	b.Resources = append(b.Resources, r)

	if r.HasRole(xir.Role_Hypervisor) {
		if r.OS == nil {
			r.OS = new(xir.OSConfig)
		}
		r.OS.BGP = b.getBGP(id, false, true)
	}

	return r

}

/*
These next two functions are currently only used in testing models
to test pathfinding between devices with a VTEP on it.

These aren't expected to be actually useful in a production model,
as no realization process currently places a VTEP on a node.

In a production model, use the regular Node/Nodes builders instead
to avoid allocating BGP addresses for devices that won't use it.
*/
func (b *Builder) NodesWithBGP(
	count int, idprefix string, mods ...Modifier) []*xir.Resource {

	return resources(b.NodeWithBGP, count, idprefix, mods...)

}

func (b *Builder) NodeWithBGP(id string, mods ...Modifier) *xir.Resource {

	r := &xir.Resource{
		Id:    id,
		Alloc: []xir.AllocMode{},
		Roles: []xir.Role{
			xir.Role_TbNode,
		},
		OS: &xir.OSConfig{
			BGP: []*xir.BGPRouterConfig{
				{
					Vrf: "ifabric",
					ASN: b.Underlay["ifabric"].asn,
					Interfaces: []*xir.InterfaceConfig{
						{
							Address:   b.Underlay["ifabric"].ip.String(),
							Interface: "lo",
						},
					},
				},
				{
					Vrf: "xfabric",
					ASN: b.Underlay["xfabric"].asn,
					Interfaces: []*xir.InterfaceConfig{
						{
							Address:   b.Underlay["xfabric"].ip.String(),
							Interface: "lo",
						},
					},
				},
			},
		},
	}

	b.Underlay["ifabric"].IncBGP()
	b.Underlay["xfabric"].IncBGP()

	for _, mod := range mods {
		mod.Modify(r)
	}

	b.Resources = append(b.Resources, r)

	return r

}

func (b *Builder) Facility() *xir.Facility {

	// commit cache to disk
	err := b.Cache.Commit()
	if err != nil {
	}

	return &xir.Facility{
		Id:        b.Id,
		Fqdn:      b.FQDN,
		Resources: b.Resources,
		Cables:    b.Cables,
	}
}

func (b *Builder) GetResource(id string) *xir.Resource {

	for _, x := range b.Resources {
		if x.Id == id {
			return x
		}
	}

	return nil

}

func resources(
	constructor func(string, ...Modifier) *xir.Resource,
	count int, idprefix string, mods ...Modifier) []*xir.Resource {

	var rs []*xir.Resource

	format := "%s%d"
	if IndexPad > 0 {
		format = "%s%0" + fmt.Sprintf("%d", IndexPad) + "d"
	}

	for i := 0; i < count; i++ {
		name := fmt.Sprintf(format, idprefix, i)
		r := constructor(name, mods...)
		rs = append(rs, r)
	}

	return rs

}
