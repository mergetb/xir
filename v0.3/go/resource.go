package xir

import (
	"encoding/base64"
	"fmt"
	"io/ioutil"
	"log"
	"strings"

	"github.com/golang/protobuf/proto"
)

func (f *Facility) Lift() *Topology {

	ifxmap := make(map[string]*Interface)
	edgemap := make(map[string]*Edge)

	t := new(Topology)

	// build maps

	for _, r := range f.Resources {

		d := &Device{Data: r}

		i := uint32(0)
		for _, n := range r.NICs {
			for _, p := range n.Ports {
				p.Parent = r.Id
				p.Index = i
				if p.Breakout == nil || p.Breakout.Index == p.Breakout.Radix-1 {
					i++
				}

				ifx := &Interface{
					Device: d,
					Data:   p,
				}
				d.Interfaces = append(d.Interfaces, ifx)
				ifxmap[p.Label()] = ifx

			}
		}
		d.IfxByPort = ifxmap

		t.Devices = append(t.Devices, d)

	}

	for _, c := range f.Cables {

		cnx := &Connection{Data: c}

		i := uint32(0)
		for _, e := range c.Ends {
			for _, x := range e.Connectors {
				x.Parent = c.Id
				x.Index = i
				i++

				edge := &Edge{
					Data:       x,
					Connection: cnx,
				}
				cnx.Edges = append(cnx.Edges, edge)
				edgemap[x.Label()] = edge

			}
		}

		t.Connections = append(t.Connections, cnx)
	}

	// connect topology

	for _, d := range t.Devices {
		for _, i := range d.Interfaces {
			if i.Data.(*Port).Connector != nil {
				i.Edge = edgemap[i.Data.(*Port).Connector.Label()]
				if i.Edge == nil {
					log.Fatalf("edge not found for connector %s %+v",
						i.Data.(*Port).Connector.Label(),
						i.Data.(*Port).Connector,
					)
				}
			}
		}
	}

	for _, c := range t.Connections {
		for _, e := range c.Edges {
			if e.Data.(*Connector) != nil {
				e.Interface = ifxmap[e.Data.(*Connector).Port.Label()]
				if e.Interface == nil {
					log.Fatalf("interface not found for port %s %+v from %s",
						e.Data.(*Connector).Port.Label(),
						e.Data.(*Connector).Port,
						c.Cable().Id,
					)
				}
			}
		}
	}

	return t

}

func (r *Resource) Device() *Device {

	d := &Device{Data: r}
	for _, x := range r.NICs {
		for _, p := range x.Ports {
			d.Interfaces = append(d.Interfaces, &Interface{
				Data:   p,
				Device: d,
			})
		}
	}

	return d

}

func Connect(p *Port, c *Connector) {
	p.Connector = &Ref{Element: c.Parent, Index: c.Index}
	c.Port = &Ref{Element: p.Parent, Index: p.Index}
}

func (r *Resource) Identify() string {
	return r.Id
}

func (r *Resource) Degree() int {

	d := 0

	for _, n := range r.NICs {
		d += len(n.Ports)
	}

	return d

}

func (r *Resource) Xdegree() int { return r.degree(LinkRole_XpLink) }
func (r *Resource) Idegree() int { return r.degree(LinkRole_InfraLink) }

func (r *Resource) degree(role LinkRole) int {

	d := 0

	for _, n := range r.NICs {
		for _, p := range n.Ports {
			if p.Role == role {
				d++
			}
		}
	}

	return d

}

func (r *Resource) GetBGP() []*BGPRouterConfig {

	if r == nil {
		return nil
	}

	if r.OS == nil {
		return nil
	}

	if r.OS.BGP == nil {
		return nil
	}

	return r.OS.BGP

}

func (r *Resource) GetEVPN() []*EVPNConfig {

	bgp := r.GetBGP()

	if bgp == nil {
		return nil
	}

	var result []*EVPNConfig

	for _, x := range bgp {
		if x.EVPN != nil {
			result = append(result, x.EVPN)
		}
	}

	return result

}

// GetPf: returns PF-port for the given port, or nil if it can't be found
// If port is a PF itself, GetPf returns it
func (r *Resource) GetPf(port *Port) *Port {
	if port.Sriov == nil || port.Sriov.GetVf() == nil {
		return port // either not SRIOV or PF
	}
	if int(port.Sriov.PfNi) >= len(r.NICs) ||
		int(port.Sriov.PfPi) >= len(r.NICs[port.Sriov.PfNi].Ports) {

		log.Fatalf("GetPf port %+v pfni/pfpi are out of range", port)
		return nil
	}
	log.Printf("GetPf returned xxx: %#v", r.NICs[port.Sriov.PfNi].Ports[port.Sriov.PfPi])
	return r.NICs[port.Sriov.PfNi].Ports[port.Sriov.PfPi]

}

func (r *Resource) NextEth() *Port { return r.NextIfx(NICKind_ETH) }
func (r *Resource) NextEnp() *Port { return r.NextIfx(NICKind_ENP) }
func (r *Resource) NextEno() *Port { return r.NextIfx(NICKind_ENO) }
func (r *Resource) NextEns() *Port { return r.NextIfx(NICKind_ENS) }
func (r *Resource) NextSwp() *Port { return r.NextIfx(NICKind_SWP) }
func (r *Resource) NextSwps(count uint) []*Port {
	return r.NextIfxs(NICKind_SWP, count)
}

func (r *Resource) NextSwpsG(count uint, gbps uint64) []*Port {
	return r.NextIfxsG(gbps, NICKind_SWP, int(count))
}

func (r *Resource) NextEthsG(count uint, gbps uint64) []*Port {
	return r.NextIfxsG(gbps, NICKind_ETH, int(count))
}

func (r *Resource) NextSwpG(gbps uint64) *Port {
	return r.NextIfxG(gbps, NICKind_SWP)
}
func (r *Resource) NextEnoG(gbps uint64) *Port {
	return r.NextIfxG(gbps, NICKind_ENO)
}
func (r *Resource) NextEnpG(gbps uint64) *Port {
	return r.NextIfxG(gbps, NICKind_ENP)
}
func (r *Resource) NextEthG(gbps uint64) *Port {
	return r.NextIfxG(gbps, NICKind_ETH)
}

func (r *Resource) NextEnpsG(gbps uint64, quantity int) []*Port {
	return r.NextIfxsG(gbps, NICKind_ENP, quantity)
}

func (r *Resource) NextIfxG(gbps uint64, kind NICKind) *Port {
	return r.NextIfxsG(gbps, kind, 1)[0]
}

func (r *Resource) NextIfxsG(gbps uint64, kind NICKind, quantity int) []*Port {

	var ports []*Port

	for _, x := range r.NICs {
		if x.Kind != kind {
			continue
		}
		for _, p := range x.Ports {
			if p.Parent != r.Id {
				log.Printf("%s !! %s (%s)", r.Id, p.Parent, p.Label())
			}
			if p.Capacity != Gbps(gbps) {
				continue
			}
			if p.Connector == nil {
				ports = append(ports, p)
				if len(ports) == quantity {
					break
				}
			}
		}
	}
	if len(ports) < quantity {
		panic(
			fmt.Sprintf(
				"[%s] insufficient ports available %d found %d needed",
				r.Id, len(ports), quantity,
			),
		)
	}
	return ports
}

func (r *Resource) Infranet() *Port {
	for _, x := range r.NICs {
		for _, p := range x.Ports {
			if p.Role == LinkRole_InfraLink {
				return p
			}
		}
	}
	panic("infralink not available")
}

func (r *Resource) Mgmt() *Port {
	for _, x := range r.NICs {
		for _, p := range x.Ports {
			if p.Role == LinkRole_MgmtLink {
				return p
			}
		}
	}
	panic("mgmtlink not available")
}

func (r *Resource) Emu() *Port {
	for _, x := range r.NICs {
		for _, p := range x.Ports {
			if p.Role == LinkRole_EmuLink {
				return p
			}
		}
	}
	panic("emulink not available")
}

func (r *Resource) NextXpnet() *Port {
	for _, x := range r.NICs {
		for _, p := range x.Ports {
			if p.Role == LinkRole_XpLink {
				if p.Connector == nil {
					return p
				}
			}
		}
	}
	panic("xplink not available")
}

func (r *Resource) NextXpnetG(gbps uint64) *Port {
	for _, x := range r.NICs {
		for _, p := range x.Ports {
			if p.Role == LinkRole_XpLink && p.Capacity == Gbps(gbps) {
				if p.Connector == nil {
					return p
				}
			}
		}
	}
	panic("xplink not available")
}

func (r *Resource) NextIfx(kind NICKind) *Port {

	for _, x := range r.NICs {
		if x.Kind != kind {
			continue
		}
		for _, p := range x.Ports {
			if p.Connector == nil {
				return p
			}
		}
	}
	panic(fmt.Sprintf("%s not available", kind))

}

func (r *Resource) NextIfxs(kind NICKind, count uint) []*Port {

	for _, x := range r.NICs {
		if x.Kind != kind {
			continue
		}
		for i, p := range x.Ports {
			if p.Connector == nil {
				return x.Ports[i : i+int(count)]
			}
		}
	}
	panic(fmt.Sprintf("%s not available", kind))

}

func (r *Resource) Eth(i int) *Port {

	var eth []*Port
	for _, x := range r.NICs {
		if x.Kind != NICKind_ETH {
			continue
		}
		eth = append(eth, x.Ports...)
	}
	return eth[i]

}

func (r *Resource) Swp(i int) *Port {

	var eth []*Port
	for _, x := range r.NICs {
		if x.Kind != NICKind_SWP {
			continue
		}
		eth = append(eth, x.Ports...)
	}
	return eth[i]

}

func (r *Resource) Swps() []*Port {

	var swp []*Port
	for _, x := range r.NICs {
		if x.Kind != NICKind_SWP {
			continue
		}
		swp = append(swp, x.Ports...)
	}
	return swp

}

func (r *Resource) SwpIndex(i uint32) *Port {

	for _, x := range r.NICs {
		if x.Kind != NICKind_SWP {
			continue
		}

		for _, p := range x.Ports {
			if p.Index == i {
				return p
			}
		}
	}

	return nil

}

func (r *Resource) Gbps(v uint64) []*Port {

	var ps []*Port

	for _, n := range r.NICs {
		for _, p := range n.Ports {
			if p.Capacity == Gbps(v) {
				ps = append(ps, p)
			}
		}
	}

	return ps

}

func (r *Resource) PortSelect(selector func(*Port) bool) map[string]*Port {

	result := make(map[string]*Port)

	for _, n := range r.NICs {
		for _, p := range n.Ports {
			if selector(p) {
				name := r.PortName(p)
				result[name] = p
			}
		}
	}

	return result

}

func (ifx *Interface) PortName() string {

	return ifx.Device.Resource().PortName(ifx.Port())

}

func (ifx *Interface) PhysPortName() string {

	return ifx.Device.Resource().PhysPortName(ifx.Port())

}

func (r *Resource) PhysPortName(x *Port) string {
	return r.portName(x, true)
}

func (r *Resource) PortName(x *Port) string {
	return r.portName(x, false)
}

func (r *Resource) portName(x *Port, physonly bool) string {

	if !physonly {

		if x.Bond != nil {
			return x.Bond.Name
		}

	}

	if x.Name != "" {
		return x.Name
	}

	kind_index := make(map[NICKind]uint32)

	for _, n := range r.NICs {
		for _, p := range n.Ports {
			if p == x {
				idx := n.StartingIndex + kind_index[n.Kind]
				kind_name := NICKind_name[int32(n.Kind)]
				if p.Sriov != nil && p.Sriov.GetVf() != nil {
					vf := p.Sriov.GetVf()
					pfPort := r.GetPf(p)
					if pfPort == nil || pfPort.Sriov == nil || pfPort.Sriov.GetPf() == nil {
						log.Fatal("portName: pfPort isn't type PF")
					}
					pfName := r.portName(pfPort, true) // get name for the parent
					vfName := ""
					if pfName != "" {
						vfName = fmt.Sprintf("%s_%d", pfName, vf.VfIndex)
					}
					return vfName
				}
				if p.Breakout != nil {
					return strings.ToLower(
						fmt.Sprintf("%s%ds%d", kind_name, idx, p.Breakout.Index),
					)
				}

				return strings.ToLower(
					fmt.Sprintf("%s%d", kind_name, idx),
				)
			}

			// increment port counter if not a breakout port, or last subport of a breakout port
			if p.Breakout == nil || p.Breakout.Index == p.Breakout.Radix-1 {
				kind_index[n.Kind]++
			}
		}
	}

	return ""
}

func (c *Connector) Equals(x *Connector) bool {
	return c.Parent == x.Parent && c.Index == x.Index
}

func (r *Resource) PortsByRole(role LinkRole) map[string]*Port {

	return r.PortSelect(func(p *Port) bool { return p.Role == role })

}

func (x NICKind) Prefix() string {

	return strings.ToLower(x.String())

}

func (r *Resource) HasAllocMode(modes ...AllocMode) bool {

	for _, mode := range modes {
		for _, x := range r.Alloc {
			if x == mode {
				return true
			}
		}
	}

	return false
}

func (r *Resource) HasRole(roles ...Role) bool {

	if r == nil {
		return false
	}

	for _, role := range roles {
		for _, x := range r.Roles {
			if x == role {
				return true
			}
		}
	}

	return false

}

func (r *Resource) HasRoles(roles ...Role) bool {

	if r == nil {
		return false
	}

	found := 0

	for _, role := range roles {
		for _, x := range r.Roles {
			if x == role {
				found++
			}
			if found == len(roles) {
				return true
			}
		}
	}

	return false

}

func (r *Resource) IsSwitch() bool {
	return r.HasRole(Role_XpSwitch, Role_InfraSwitch)
}

// Resource.TotalCores() returns the resource total number of cores
func (r *Resource) TotalCores() uint32 {
	c := uint32(0)
	for _, p := range r.Procs {
		c += p.Cores
	}
	return c
}

// Resource.ReservedCores() returns the resource reserved cores
func (r *Resource) ReservedCores() uint32 {
	c := uint32(0)
	for _, p := range r.Procs {
		c += p.ReservedCores
	}
	return c
}

// Resource.Cores() returns the resource total commissioned cores
// same as Resouce.TotalCores() - Resource.ReservedCores()
func (r *Resource) Cores() uint32 {
	c := uint32(0)
	for _, p := range r.Procs {
		c += p.Cores - p.ReservedCores
	}
	return c
}

// Resource.TotalMem() returns the resource total memory capacity
func (r *Resource) TotalMem() uint64 {
	c := uint64(0)
	for _, dimm := range r.Memory {
		c += dimm.Capacity
	}
	return c
}

// Resource.ReservedMem() returns the resource reserved memory capacity
func (r *Resource) ReservedMem() uint64 {
	c := uint64(0)
	for _, dimm := range r.Memory {
		c += dimm.ReservedCapacity
	}
	return c
}

// Resource.Mem() returns the resource total commissioned memory capacity
// same as Resouce.TotalMem() - Resource.ReservedMem()
func (r *Resource) Mem() uint64 {
	c := uint64(0)
	for _, dimm := range r.Memory {
		c += dimm.Capacity - dimm.ReservedCapacity
	}
	return c
}

func (r *Resource) Storage() uint64 {
	c := uint64(0)
	for _, disk := range r.Disks {
		c += disk.Capacity
	}
	return c
}

func (r *Resource) XpBandwidth() uint64 {
	c := uint64(0)
	for _, nic := range r.NICs {
		for _, p := range nic.Ports {
			if p.Role == LinkRole_XpLink {
				c += p.Capacity
			}
		}
	}
	return c
}

func (r *Resource) InfraBandwidth() uint64 {
	c := uint64(0)
	for _, nic := range r.NICs {
		for _, p := range nic.Ports {
			if p.Role == LinkRole_InfraLink {
				c += p.Capacity
			}
		}
	}
	return c
}

func (r *Resource) SwpBandwidth() uint64 {
	c := uint64(0)
	for _, nic := range r.NICs {
		if nic.Kind != NICKind_SWP {
			continue
		}
		for _, p := range nic.Ports {
			c += p.Capacity
		}
	}
	return c
}

// Break out a port into sub-ports
func (n *NIC) Breakout(p *Port, ps []*Port) []*Port {

	var ports, newports []*Port

	for _, x := range n.Ports {
		if x != p {
			ports = append(ports, x)
		} else {
			for i := range ps {
				z := new(Port)
				z.Parent = p.Parent
				z.Index = p.Index
				z.Role = p.Role
				z.Breakout = &Breakout{Index: uint32(i), Radix: uint32(len(ps))}
				newports = append(newports, z)
				ports = append(ports, z)
			}
		}
	}

	n.Ports = ports

	return newports

}

func (n *NIC) PhysPort(index int) *Port {

	/*
		idx := 0
		for _, x := range n.Ports {
			if idx == index && x.Breakout == nil {
				return x
			}
			if x.Breakout != nil {
				if x.Breakout.Index == x.Breakout.Radix-1 {
					idx++
				}
			} else {
				idx++
			}
		}
	*/
	for _, x := range n.Ports {
		if x.Index == uint32(index) {
			return x
		}
	}

	return nil

}

// Collect a range of physical ports, end is non-inclusive
func (n *NIC) PhysPorts(begin, end int) []*Port {
	var result []*Port
	for i := begin; i < end; i++ {
		result = append(result, n.PhysPort(i))
	}
	return result
}

// SupportsVST is a predicate on NIC supporting VLAN switch tagging
// e.g ip link set dev <PF device> vf <NUM> vlan <vlan_id> [qos <qos>]
// more details here:
// https://docs.nvidia.com/networking/pages/viewpage.action?pageId=19801751#SingleRootIOVirtualization(SRIOV)-VLANGuestTagging(VGT)andVLANSwitchTagging(VST)
// NOTE: currently unused until we find non-compliant hardware, then we
// can use it while defining facility topology to throw errors
func (n *NIC) SupportsVST() bool {
	switch n.GetModel() {
	case NICModel_ConnectX4, NICModel_ConnectX5, NICModel_ConnectX6:
		return true
	}
	//we're being concervative here, should check first and add/update a case statement above
	return false
}

func (p *Port) Label() string {
	suffix := ""
	if p.Breakout != nil {
		suffix = fmt.Sprintf("_%d", p.Breakout.Index)
	}
	return fmt.Sprintf("%s.%d%s", p.Parent, p.Index, suffix)
}

func (c *Connector) Label() string {
	return fmt.Sprintf("%s.%d", c.Parent, c.Index)
}

func FacilityFromFile(file string) (*Facility, error) {

	buf, err := ioutil.ReadFile(file)
	if err != nil {
		return nil, err
	}

	return FacilityFromB64String(string(buf))

}

// TODO all these serialization functions need to be guarded by XIR semvers to
// avoid subtle and very difficult to track bugs from creeping in due to protobuf
// spec skew or protoc versions.
func FacilityFromB64String(pbuf string) (*Facility, error) {

	data, err := base64.StdEncoding.DecodeString(pbuf)
	if err != nil {
		return nil, err
	}

	return FacilityFromRawString(string(data))

}

func FacilityFromRawString(pbuf string) (*Facility, error) {

	n := new(Facility)
	return n, proto.Unmarshal([]byte(pbuf), n)

}

func (f *Facility) ToB64Buf() ([]byte, error) {

	buf, err := proto.Marshal(f)
	if err != nil {
		return nil, err
	}

	encoded := base64.StdEncoding.EncodeToString(buf)

	return []byte(encoded), nil

}

func (f *Facility) ToBuf() ([]byte, error) {

	return proto.Marshal(f)

}

func (f *Facility) Clone() (*Facility, error) {

	if f == nil {
		return nil, nil
	}

	buf, err := f.ToBuf()
	if err != nil {
		return nil, err
	}

	return FacilityFromRawString(string(buf))

}

func (f *Facility) Resource(id string) *Resource {

	for _, x := range f.Resources {

		if x.Id == id {
			return x
		}

	}

	return nil

}

func (f *Facility) Cable(id string) *Cable {

	for _, x := range f.Cables {

		if x.Id == id {
			return x
		}

	}

	return nil

}

func (d *Disk) HasRole(r DiskRole) bool {

	for _, x := range d.Roles {
		if x == r {
			return true
		}
	}

	return false

}

func (r *Resource) DiskName(d *Disk) string {

	nvmeIndex := make(map[uint32]int)

	for i, _d := range r.Disks {

		switch d.DiskInterface {
		case DiskInterface_NVMEx2, DiskInterface_NVMEx4:
			nvmeIndex[_d.NVMEControllerIndex]++
		}

		if d == _d {
			switch d.DiskInterface {
			case DiskInterface_SATA1, DiskInterface_SATA2, DiskInterface_SATA3, DiskInterface_SAS:
				return "sd" + diskAlphabetSuffix(uint(i))
			case DiskInterface_NVMEx2, DiskInterface_NVMEx4:
				return fmt.Sprintf("nvme%dn%d", d.NVMEControllerIndex, nvmeIndex[d.NVMEControllerIndex])
			case DiskInterface_VirtioBlock:
				return "vd" + diskAlphabetSuffix(uint(i))
			}
		}

	}

	return ""

}

func (d *Disk) DevName(i uint) string {

	switch d.DiskInterface {
	case DiskInterface_SATA1, DiskInterface_SATA2, DiskInterface_SATA3, DiskInterface_SAS:
		return "sd" + diskAlphabetSuffix(i)
	case DiskInterface_NVMEx2, DiskInterface_NVMEx4:
		return fmt.Sprintf("nvme%d", i)
	case DiskInterface_VirtioBlock:
		return "vd" + diskAlphabetSuffix(i)
	}

	return ""

}

func diskAlphabetSuffix(i uint) string {

	switch {
	case i < 26:
		return fmt.Sprintf("%c", 'a'+i)
	case i < 702:
		return fmt.Sprintf("%c%c", 'a'+i/25, 'a'+i%26)
	case i < 18954:
		return fmt.Sprintf(
			"%c%c%c",
			'a'+(i/25)/25,
			'a'+(i/25)%26,
			'a'+i%26,
		)
	}

	panic("alphabet suffix overflow")
}

func (x *Cable) IsBreakout() bool {
	switch x.Kind {
	case CableKind_DACBreakout, CableKind_AOCBreakout, CableKind_FiberMPOBreakout, CableKind_GenericBreakout:
		return true
	default:
	}

	return false
}
